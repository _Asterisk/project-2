#include "hashtbl.h"
#include <iostream>
using std::endl;
using std::ostream;
#include <string.h> /* for strcpy */
#include <algorithm> /* for find */
using std::find;
using std::swap;
#include <iomanip>
using std::setfill;
using std::setw;
#include <cassert>

#define R32 (rand()*100003 + rand())
#define R64 (((uint64_t)R32 << 32) + R32)
#define TLEN ((size_t)(1 << this->nBits))

namespace csc212
{
	hashTbl::hash::hash(unsigned rangebits, const uint32_t* a,
					const uint64_t* alpha, const uint64_t* beta) {
		this->rangebits = rangebits;
		if (a) {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = a[i] | 1; /* make sure it is odd. */
			}
		} else {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = R32 | 1;
			}
		}
		this->alpha = ((alpha) ? *alpha : R64) | 1;
		this->beta = ((beta) ? *beta : R64);
		this->beta &= ((uint64_t)-1)>>(64 - rangebits);
	}

	uint64_t hashTbl::hash::operator()(const string& x) const {
		assert(x.length() <= 4*aLen);

		const uint32_t* S = reinterpret_cast<const uint32_t*>(x.c_str());

		uint64_t a1,a2,result,product=0;
		size_t k = ((x.length()+1)/4);

		if ((k^1) == (k-1)){
	//		k++;
			for (size_t i = 0; i < (k/2)+1; i++){
				a1 = S[2*i] + this->a[2*i];
				a2 = S[2*i +1] + this->a[2*i +1];
				product += a1*a2;
				}
			}

		if ((k^1) < (k-1)){
			for (size_t i = 0; i <(k/2)+1; i++){
				a1 = S[2*i] + this->a[2*i];
				a2 = S[2*i +1] + this->a[2*i +1];
				product += a1*a2;
				}
			}

		if ((k^1) > (k-1)){
			for (size_t i = 0; i < (k/2); i++){
				a1 = S[2*i] + this->a[2*i];
				a2 = S[2*i +1] + this->a[2*i +1];
				product += a1*a2;
				}
			}


			result = ((alpha*product)+beta)>>(64 - rangebits );
			return result;
			}


	//constructors:
	hashTbl::hashTbl(unsigned nBits, const uint32_t* ha,
					const uint64_t* halpha, const uint64_t* hbeta) :
		nBits(nBits),h(nBits,ha,halpha,hbeta)
	{
		this->table = new list<val_type>[TLEN];
	}
	hashTbl::hashTbl(const hashTbl& H)
	{
		this->nBits = H.nBits;
		(this->h).rangebits = (H.h).rangebits;
		(this->h).alpha = (H.h).alpha;
		(this->h).beta = (H.h).beta;
		this->table  = new list<val_type>[TLEN];
		for (size_t i = 0; i < TLEN; i++){
			this->table[i] = H.table[i];
			}

	}
	hashTbl::~hashTbl()
	{
		delete[] this->table;
	}

	//operators:
	hashTbl& hashTbl::operator=(hashTbl H)
	{
	swap(this->table, H.table);
	this->nBits = H.nBits;
	this->h = H.h;
	return *this;
	}

	ostream& operator<<(ostream& o, const hashTbl& H)
	{
		for (size_t i = 0; i < H.tableLength(); i++) {
			o << "[" << setfill('0') << setw(2) << i << "] |";
			for (list<val_type>::iterator j = H.table[i].begin();
					j != H.table[i].end(); j++) {
				o << *j << "|";
			}
			o << endl;
		}
		return o;
	}

	void hashTbl::insert(val_type x)
	{
		if (!search(x)){
			table[h(x)].push_back(x);
			}
	}

	void hashTbl::remove(val_type x)
	{
		for (size_t i = 0; i <TLEN; i++){
			if (find(table[i].begin(),table[i].end(),x)!=table[i].end()){
				table[i].remove(x);
				}
			}
	}

	void hashTbl::clear()
	{
		for(size_t i=0; i<TLEN; i++)
			this->table[i].clear();
	}

	bool hashTbl::isEmpty() const
	{
		for(size_t i = 0; i<TLEN; i++){
			if(table[i].size() > 0){
				return false;
				}
			}
		return true;
	}

	bool hashTbl::search(val_type x) const
	{
		for(size_t i = 0; i< TLEN; i++){
			if (find(table[i].begin(),table[i].end(),x) != table[i].end()){
				return true;
				}
			}
		return false;
		}


	size_t hashTbl::countElements() const
	{
		size_t count = 0;
		for (size_t i = 0; i <TLEN; i++){
			count += table[i].size();
			}
		return count;
	}

	size_t hashTbl::tableLength() const
	{
		return TLEN;
	}

	size_t hashTbl::countCollisions() const
	{
		size_t i,nCollisions = 0;
		for(i=0; i<TLEN; i++){
			if(table[i].size() > 1){
				++nCollisions;
			}
		}
		return nCollisions;
	}

	size_t hashTbl::longestListLength() const
	{
		size_t i, length = 0 ;
		for ( i = 0; i <TLEN; i++){
			if(table[i].size() > length){
				length = table[i].size()-1;
				}
			}
		return length;
	}
}